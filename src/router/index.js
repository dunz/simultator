import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import Extends from '../components/Extends';
import Mixins from '../components/Mixins';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'main',
            component: Main
        },
        {
            path: '/extends',
            name: 'extends',
            component: Extends
        },
        {
            path: '/mixins',
            name: 'mixins',
            component: Mixins
        }
    ]
});
