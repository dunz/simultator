# simulator

> 궁금증 해결 실험기

# 프로젝트 구성
> vue init webpack simulator -y 로 세팅

프론트엔드 기술적 스펙 테스트를 추후 계속 업데이트 해 나갈 예정

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
